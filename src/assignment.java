import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class assignment {
    private JPanel root;
    private JButton tempuraButton;
    private JButton karaageButton;
    private JButton gyozaButton;
    private JButton udonButton;
    private JButton yakinikuButton;
    private JButton ramenButton;
    private JButton checkOutButton;
    private JTextPane textPane1;
    private JLabel totallabel;
    private JTextPane textPane2;

    int total=0;

    void order(String food) { //メソッド
        int confirmation = JOptionPane.showConfirmDialog(null,
                "Would you like to order" + food + "?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION);
        if (confirmation == 0) {//はいを押した
            JOptionPane.showConfirmDialog(null,
                    "Thank you for ordering "+food+"! It will be served as soon as possible.");
            textPane1.setText(textPane1.getText()+food+"\n");
            total+=100;
            totallabel.setText(""+total);
        }
    }

    public assignment() {
        tempuraButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Tempura");
            }
        });
        karaageButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Karaage");
            }
        });
        gyozaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Gyoza");
            }
        });
        udonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Udon");
            }
        });
        yakinikuButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Yakiniku");
            }
        });
        ramenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Ramen");
            }
        });
        checkOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(null,
                        "Would you like to checkout?",
                        "Checkout Confirmation",
                        JOptionPane.YES_NO_OPTION);

                if (confirmation == 0) {//はいを押した
                    JOptionPane.showConfirmDialog(null,
                            "Thank you. The total price is "+total+" yen.");
                    textPane1.setText("");
                    total=0;
                    totallabel.setText(" "+total);
                }
            }
        });
    }


    public static void main(String[] args) {
        int total=0;
        JFrame frame = new JFrame("assignment");
        frame.setContentPane(new assignment().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
